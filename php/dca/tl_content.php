<?php

/*
 * This file is part of vendor/bundle
 *
 * (c) Your name <your@email.com>
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_table_name'] = array
(

    /**
     * Config
     * https://docs.contao.org/books/api/dca/reference.html#table-configuration
     */    
	'config' => array
	(
        'label' => "", // &$GLOBALS['TL_LANG'] (string)
		'dataContainer'               => 'Table',
		// 'enableVersioning'            => true,
        // 'ptable'                      => 'tl_parent_table',
        // 'ctable'                      => ['tl_ctable_1', 'tl_ctable_2'],
		// 'dynamicPtable'               => true,
        // 'markAsCopy'                  => 'headline',
        'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid,ptable,invisible,sorting' => 'index'
			)
        ),
        /**
         * Global callbacks
         * https://docs.contao.org/books/api/dca/callbacks.html#global-callbacks
         */

        // Is executed when the DataContainer object is initialized. Allows you to e.g. check permissions or
        // to modify the Data Container Array dynamically at runtime.
		'onload_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),

        // Is executed when a new record is created.
		'oncreate_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),
        
        // Is executed when a back end form is submitted. Allows you to e.g. modify the form data before
        // it is written to the database (used to calculate intervals in the calendar extension).
		'onsubmit_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),

        // Is executed before a record is removed from the database.
		'ondelete_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),
        
        // Is executed after a record has been moved to a new position.
		'oncut_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),
        
        // Is executed after a record has been duplicated.
		'oncopy_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),
        
        // Is executed after the old version of the record has been added to tl_version.
		'oncreate_version_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),

        // As oncreate_version_callback but deprecated and with different signature.
		'onversion_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),
        
        // Is executed after a record has been restored from an old version.
		'onrestore_version_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        ),
        
        // As onrestore_version_callback but deprecated and with different signature.
		'onrestore_callback'             => array
		(
			// array('CLASS_OR_SERVICE', 'METHOD')
        )
		
	),

	/**
     * List
     * https://docs.contao.org/books/api/dca/reference.html#listing-records
     */    
	'list' => array
    (
        /**
         * List -> Sorting
         * https://docs.contao.org/books/api/dca/reference.html#sorting
         */
		'sorting' => array
		(
            'mode'                    => 1, 
            /*
            Sorting mode (integer)
            0 Records are not sorted
            1 Records are sorted by a fixed field
            2 Records are sorted by a switchable field
            3 Records are sorted by the parent table
            4 Displays the child records of a parent record (see style sheets module)
            5 Records are displayed as tree (see site structure)
            6 Displays the child records within a tree structure (see articles module)
            */
			
            // 'flag'                  => 1,
            /*
            Sorting flag (integer) [1,2,3,4,5,6,7,8,9,10,11,12]
            1 Sort by initial letter ascending
            2 Sort by initial letter descending
            3 Sort by initial two letters ascending
            4 Sort by initial two letters descending
            5 Sort by day ascending
            6 Sort by day descending
            7 Sort by month ascending
            8 Sort by month descending
            9 Sort by year ascending
            10 Sort by year descending
            11 Sort ascending
            12 Sort descending
            */
            
            // 'panelLayout'             => 'filter;search,limit',
            /**
             * Panel layout (string) [search|sort|filter|limit]
             * search: show the search records menu
             * sort: show the sort records menu 
             * filter: show the filter records menu 
             * limit: show the limit records menu
             * Separate options with comma (= space) and semicolon (= new line) like sort,filter;search,limit.
             */


            // 'fields'                  => array('sorting'),
            /**
             * Default sorting values (array)
             * One or more fields that are used to sort the table.
             */
            
            
            // 'headerFields'            => array(),
            /**
             * Default sorting values (array)
             * One or more fields that are used to sort the table.
             */


            // 'icon'                    => "myicon.png"
            /**
             * Tree icon (string)
             * Path to an icon that will be shown on top of the tree (sorting mode 5 and 6 only).
             */


            // 'root'                    => array(1,2,3)
            /**
             * Root nodes (array)
             * IDs of the root records (pagemounts). This value usually takes care of itself.
             */


            // 'rootPaste'                    => false
            /**
             * true/false (boolean)
             * Override disabling paste buttons to root, if root is set. (default: false)
             */


            // 'filter'                    => array()
            /**
             * Query filter (array)
             * Allows you to add custom filters as arrays, e.g. array('status=?', 'active').
             */


            // 'disableGrouping'                    => false
            /**
             * true/false (boolean)
             * Allows you to disable the group headers in list view and parent view.
             */



            /**
             * Listing callbacks
             * https://docs.contao.org/books/api/dca/callbacks.html#listing-callbacks
             */

            // CALLBACKS

            // 'paste_button_callback'   => array('CLASS_OR_SERVICE', 'METHOD'),
            /**
             * Allows for individual paste buttons and is e.g. used in the site structure
             * to disable buttons depending on the user's permissions (requires an additional
             * command check via load_callback).
             */
            
            // 'child_record_callback'   => array('CLASS_OR_SERVICE', 'METHOD'),
            /** 
             * Defines how child elements are rendered in "parent view". 
             */


            // 'header_callback'   => array('CLASS_OR_SERVICE', 'METHOD'),
            /**
             * Allows for individual labels in header of "parent view".
             */
            

        ),
        
        /**
         * List -> Labels
         * https://docs.contao.org/books/api/dca/reference.html#labels
         */
        'label' => array
        (
            // 'fields' => array(),
            /**
             * Fields (array)
             * One or more fields that will be shown in the list (e.g. array('title', 'user_id:tl_user.name')).
             */

            // 'showColumns' => false,
            /**
             * true/false (boolean)
             * If true Contao will generate a table header with column names (e.g. back end member list)
             */

            // 'format'=> "<span>%s</span>",
            /**
             * Format string (string)
             * HTML string used to format the fields that will be shown (e.g. %s ).
             */


            // 'maxCharacters'=> 5,
            /**
             * Number of characters (integer)
             * Maximum number of characters of the label.
             */


            /**
             * Listing callbacks
             * https://docs.contao.org/books/api/dca/callbacks.html#listing-callbacks
             */

            // CALLBACKS

            // 'group_callback'   => array('CLASS_OR_SERVICE', 'METHOD'),
            /**
             * Allows for individual group headers in the listing.
             */

             // 'label_callback'   => array('CLASS_OR_SERVICE', 'METHOD'),
            /**
             * Allows for individual labels in the listing and is e.g. used in the user module to add status icons.
             */
        



        ),

        /**
         * Global Operations
         * https://docs.contao.org/books/api/dca/reference.html#operations
         */
		'global_operations' => array
		(
			'all' => array
			(
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                /**
                 * &$GLOBALS['TL_LANG'] (string)
                 * Button label. Typically a reference to the global language array.
                 */

                'href'                => 'act=select',
                /**
                 * URL fragment (string)
                 * URL fragment that is added to the URI string when the button is clicked (e.g. act=editAll).
                 */
                
                'class'               => 'header_edit_all',
                /**
                 * CSS class (string)
                 * CSS class attribute of the button.
                 */

                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
                /**
                 * Additional attributes (string)
                 * Additional attributes like event handler or style definitions.
                 */

                 // 'button_callback'     => array('CLASS_OR_SERVICE', 'METHOD')
                 /**
                  * Callback function (array)
                  * Call a custom function instead of using the default button function.
                  * Please specify as array('Class', 'Method').
                  */
			)
        ),

        /**
         * Regular Operations (Operations per Record)
         * https://docs.contao.org/books/api/dca/reference.html#operations
         */
		'operations' => array
		(
            /**
             * Operations callbacks
             * https://docs.contao.org/books/api/dca/callbacks.html#operations-callbacks
             */
			'edit' => array
			(
                'label'               => &$GLOBALS['TL_LANG']['tl_table_name']['edit'],
                /**
                 * &$GLOBALS['TL_LANG'] (string)
                 * Button label. Typically a reference to the global language array.
                 */

                'href'                => 'act=edit',
                 /**
                 * URL fragment (string)
                 * URL fragment that is added to the URI string when the button is clicked (e.g. act=editAll).
                 * 
                 * edit:    act=edit
                 * copy:    act=paste&amp;mode=copy
                 * cut:     act=paste&amp;mode=cut
                 * delte:   act=delete
                 * toggle:  --empty--
                 * show:    act=show
                 */

                'icon'                => 'edit.svg',
                /**
                 * Icon (string)
                 * Path and filename of the icon.
                 */

                'attributes'          => '',
               
                /**
                 * Additional attributes (string)
                 * Additional attributes like event handler or style definitions.
                 * 
                 * just if you want to keep scroll position:
                 *  'onclick="Backend.getScrollOffset()"'
                 * 
                 * for a delete button:
                 *  'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
                 *
                 * for a toggle button:
                 *  'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"'
                 * 
                 */

			),
			
			
		),
	),

    /**
     * Palettes
     * https://docs.contao.org/books/api/dca/palettes.html
     */

	// Palettes
	'palettes' => array
	(
		'__selector__'                => array('type','mySubPaletteSelector'),
		'default'                     => '{type_legend},type,mySubPalette',
	),

	// Subpalettes
	'subpalettes' => array
	(
		'mySubPaletteSelector'        => 'other,fields,come,here',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'ptable' => array
		(
			'sql'                     => "varchar(64) NOT NULL default ''"
		),
		'sorting' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        // REMOVE UNWANTED FIELDS
		'type' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_table_name']['type'],
			'default'                 => 'text',
			'exclude'                 => true,
			'filter'                  => true,
            'inputType'               => 'select',
            // 'options_callback'        => array('CLASS_OR_SERVICE', 'METHOD'),
            // 'input_field_callback'    => array('CLASS_OR_SERVICE', 'METHOD'),
            // 'load_callback'           =>array('CLASS_OR_SERVICE', 'METHOD'), 
            // 'save_callback'           => array('CLASS_OR_SERVICE', 'METHOD'),
			'eval'                    => array('helpwizard'=>true, 'chosen'=>true, 'submitOnChange'=>true, 'tl_class'=>'w50'),
			'sql'                     => "varchar(64) NOT NULL default ''"
		),
        
        // PLEASE ADD YOUR FIELDS HERE

		'invisible' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_table_name']['invisible'],
			'exclude'                 => true,
			'filter'                  => true,
            'inputType'               => 'checkbox',
			'sql'                     => "char(1) NOT NULL default ''"
        ),
        
		'start' => array
		(
			'exclude'                 => true,
			'label'                   => &$GLOBALS['TL_LANG']['tl_table_name']['start'],
            'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
		),
		'stop' => array
		(
			'exclude'                 => true,
			'label'                   => &$GLOBALS['TL_LANG']['tl_table_name']['stop'],
            'inputType'               => 'text',
			'eval'                    => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
			'sql'                     => "varchar(10) NOT NULL default ''"
        ),
        
        'myFullDcaFielda' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_table_name']['myFullDcaFielda'],
            // Field label. Typically a reference to the global language array.
        
            'default'                 => 1,
            // Default value that is set when a new record is created.
        
            'exclude'                 => true,
            // If true the field will be excluded for non-admins. It can be enabled in the user group module (allowed excluded fields).
        
            'search'                  => true,
            // If true the field will be included in the search menu (see "sorting records" -> "panelLayout").
        
            'sorting'                 => true,
            // If true the field will be included in the sorting menu (see "sorting records" -> "panelLayout").
        
            'filter'                  => true,
            // If true the field will be included in the filter menu (see "sorting records" -> "panelLayout").
        
            'flag'                    => 1,
            // Sorting mode [1,2,3,4,5,6,7,8,9,10,11,12]
            // https://docs.contao.org/books/api/dca/reference.html#fields
        
            'length'                  => 24,
            // Allows to specify the number of characters that are used to build sorting groups (flag 3 and 4).
        
            'inputType'               => 'text',
            // Field type (string)
            // text - Text field
            // password - Password field
            // textarea - Textarea
            // select - Drop-down menu
            // checkbox - Checkbox
            // radio - Radio button
            // radioTable - Table with images and radio buttons
            // imageSize - Two text fields with drop-down menu
            // inputUnit - Text field with small unit drop-down menu
            // trbl - Four text fields with a small unit drop-down menu
            // chmod - CHMOD table
            // pageTree Page tree
            // fileTree - File tree
            // tableWizard - Table wizard
            // timePeriod - Text field with drop-down menu
            // listWizard - List wizard
            // optionWizard - Option wizard
            // moduleWizard - Module wizard
            // checkboxWizard - Checkbox Wizard
            // more info: https://docs.contao.org/books/api/dca/reference.html#fields
        
            'options'                 => array(1,2,3),
            // Options of a drop-down menu or radio button menu.
        
            'options_callback'        => array('CLASS_OR_SERVICE', 'METHOD'),
            // Callback function that returns an array of options. Please specify as array('Class', 'Method').
        
            'foreignKey'              => 'tl_member.email',
            // Get options from a database table. Returns ID as key and the field you specify as value.
        
            'reference'               => &$GLOBALS['TL_LANG']['someRefArray'],
            // Array that holds the options labels. Typically a reference to the global language array.
        
            'explanation'             => &$GLOBALS['TL_LANG']['someExplanationArray'],
            // Array that holds the explanation. Typically a reference to the global language array.
        
            'input_field_callback'    => array('CLASS_OR_SERVICE', 'METHOD'),
            // Executes a custom function instead of using the default input field routine and passes the the DataContainer object and the label as arguments.
        
            'eval'                    => array(),
            // Various configuration options. See next paragraph.
            // Eval has so many options, plese see: https://docs.contao.org/books/api/dca/reference.html#evaluation
        
            'wizard'                  => array('CLASS_OR_SERVICE', 'METHOD'),
            // Call a custom function and add its return value to the input field. Please specify as array('Class', 'Method').
        
            'sql'                     => "varchar(255) NOT NULL default ''",
            // Describes data type and its database configuration. (see paragraph SQL Column Definition).
            // more info: https://docs.contao.org/books/api/dca/reference.html#sql-column-definition
        
            'relation'                => array(),
            // Describes relation to parent table (see paragraph Relations).
            // more info: https://docs.contao.org/books/api/dca/reference.html#relations
        
            'load_callback'           => array(
                                           array('CLASS_OR_SERVICE','METHOD'),
                                           array('CLASS_OR_SERVICE','METHOD'),
                                         ),
            // These functions will be called when the field is loaded. Please specify each callback function as array('Class', 'Method'). Passes the field's value and the data container as arguments. Expects the field value as return value.
        
            'save_callback'           => array(
                                           array('CLASS_OR_SERVICE','METHOD'),
                                           array('CLASS_OR_SERVICE','METHOD'),
                                         ),
            // These functions will be called when the field is saved. Please specify each callback function as array('Class', 'Method'). Passes the field's value and the data container as arguments. Expects the field value as return value. Throw an exception to display an error message.
        
        ),
	)
);

// Dynamically add the permission check and parent table (see #5241)
if (Input::get('do') == 'article' || Input::get('do') == 'page')
{
	$GLOBALS['TL_DCA']['tl_table_name']['config']['ptable'] = 'tl_article';
	$GLOBALS['TL_DCA']['tl_table_name']['config']['onload_callback'][] = array('tl_table_name_hooks', 'checkPermission');
}
