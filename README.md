# Contao Code Snippets

three types of snippets are provided:
1. function snippets for DCA callbacks
2. DCA fields and field-Types
3. a full DCA

To use functino snippets, just start writing the name of the callback type, ans select one from the provided list.
To use DCA fields, just start typing ```contao dca field``` or `cdf`
To get a full sample DCA, type `contao full dca` or `cfd`

## 1. function snippets
### operation callbacks
- button_callback
- label_callback
### listing callbacks
- child_record_callback
- paste_button_callback
- input_field_callback
- load_callback
- options_callback
- save_callback

### config callbacks
- oncopy_callback
- oncut_callback
- ondelete_callback
- onload_callback
- onsubmit_callback
## 2. DCA fields and field-Types
### simple fields
- Text
- Passwort
- Textarea
- Auswahlfeld (select)
- Clicklist (Select-Variante)
- Checkbox
- Radio-Button
- Tabelle mit Radio-Buttons (radioTable)
- Bildgröße (imageSize)
- Eingabefeld mit Einheit (inputUnit)
- Vier Eingabefelder mit Einheit (trbl)
- Tabelle für Dateirechte (chmod)
- Seitenbaum für eine Seite (pageTree)
- Seitenbaum für mehrere Seiten (pageTree)
- Dateibaum für eine Datei (fileTree)
- Dateibaum für mehrere Dateien (fileTree)
- Dateibaum für ein Verzeichnis (fileTree)
- Dateibaum für mehrere Verzeichnisse (fileTree)
- Tabelle (tableWizard)
- Liste (listWizard)
- Verwaltung von Optionen (optionWizard)
- Verwaltung von Modulen (moduleWizard)
- Sortierbare Checkbox-Liste (checkboxWizard)
### special fields
- id
- pid
- tstamp
- sorting
### field types
- Natürliche Zahl
- Zahl
- Datum
- Telefon
- E-Mail
- Postleitzahl
- Farbwähler
- Vier-Fach-Textfeld
- Land
- URL
- Mitgliederauswahl
- Templateauswahl
## 3. full DCA
To get a full sample DCA, type `contao full dca` or `cfd`

## Sample function code snippets for the following callbacks
these snippets provide function snippets for your callback classes
sampe: **button_callback**
```php
 /** 
     * button_callback: Ermöglicht individuelle Navigationssymbole
     * @param array $arrRow the current row 
     * @param string $href the url of the embedded link of the button 
     * @param string $label label text for the button 
     * @param string $title title value for the button 
     * @param string $icon url of the image for the button 
     * @param array $attributes additional attributes for the button (fetched from the array key "attributes" in the DCA) 
     * @param string $strTable the name of the current table 
     * @param $arrRootIds array of the ids of the selected "page mounts" (only in tree view) 
     * @param $arrChildRecordIds ids of the childs of the current record (only in tree view) 
     * @param boolean $blnCircularReference determines if this record has a circular reference (used to prevent pasting of an cutted item from an tree into any of it's childs). 
     * @param string $strPrevious id of the previous entry on the same parent/child level. Used for move up/down buttons. Not for root entries in tree view. 
     * @param string $strNext id of the next entry on the same parent/child level. Used for move up/down buttons. Not for root entries in tree view. 
     * @return var
     */ 
    public function myButtonCallback($arrRow, $href, $label, $title, $icon, $attributes, $strTable, $arrRootIds, $arrChildRecordIds, $blnCircularReference, $strPrevious, $strNext)
    {
        // do something
        return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> ';
    }
```
